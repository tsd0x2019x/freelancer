**Brief overview of projects**

This repository is a collection of freelance-projects that are done for public and private demands.
The following shows a list of those projects with their description.

---

## Javascript-for-simple-game (game3)

This is a project from "freelancer.com". It's about a simple memory card game realized in HTML5 and
Javascript. The game has a timer and two rows of cards. The timer starts with 30 seconds and counts
down. At the beginning, the player must choose one of the cards in the upper row. Then, he/she can
choose a card in the under row. If the upper-row card and the under-row card match, the player gets
one success point and continues with the remaining cards. Otherwise, the clicked cards will be 
faded out and the player can repeat the turn. 

---

## Periodic Sequence

This is a small project for a family's friend. Given a permutation, it calculates the rule and 
returns the result.

---