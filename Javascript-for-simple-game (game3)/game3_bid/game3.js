/**************************\
 * game3.js                |
 **************************/

// Namespace for game
var Game3 = {
    DEFAULT_CSS_BOTTOM_IMAGES : "div.row[style] div.col-3 img.img-fluid",
    DEFAULT_CSS_TOP_IMAGES : "div.row:not([style]) div.col-3 img.img-fluid", // Get all [img] elements that are inside a [div] of class "col-3" 
                                                // and descendants of (parent) [div] element, that is of class "row" and has no atribute "style".
    DEFAULT_CSS_TIMER_ID : "Timer",
    DEFAULT_TIMER_DURATION : 30, // time (seconds) to play
    DEFAULT_OPACITY : 0.6, // Opacity 60%
    DEFAULT_SUCCESS_NUMBER : 4, // Number of images/cards in each row

    top_imgs : [], /* Images in top row    => Array of bundles {Image, Index, Bool}, where the [Bool] flag indicates if the top [Image] clicked has an correspodning bottom image successfully. */
    btm_imgs : [], /* Images in bottom row => Simple array of images */
    top_img_clicked : false, /* Indicates if an top image is recently clicked or not */
    success_attempts : 0, /* Number of attempts that were successful */
    recent_top_index : -1, /* Index of the top image that is recently clicked */

    timer : null,
    timer_anim_id : null,
    timer_start_millis : Date.now(), // Milliseconds from 1/1/1970 00:00:00
    timer_time_left : 0,

    /*
     * Start the game
     */
    Start : function () {
        this.Init();
        this.Timer_Run();
    }, /* End Start */

    /*
     * Initializes the image objects in the top and bottom row.
     */
    Init : function () {
        // Get reference to timer
        this.timer = document.getElementById(Game3.DEFAULT_CSS_TIMER_ID);

        // Get references of all images in the top row.
        //this.top_imgs = document.querySelectorAll(Game3.DEFAULT_CSS_TOP_IMAGES);
        (document.querySelectorAll(Game3.DEFAULT_CSS_TOP_IMAGES)).forEach (function (img, idx) {
            Game3.top_imgs[idx] = { image: img, index: idx, success: false};
        });

       // Get references of all images in the bottom row.
       this.btm_imgs = document.querySelectorAll(Game3.DEFAULT_CSS_BOTTOM_IMAGES);

       // Add click listener to image objects in top row.
       this.top_imgs.forEach(function (el) {
            el.image.addEventListener("click", function () {
                if (Game3.Get_Opacity(el.image) < 1.0) {
                    if (Game3.recent_top_index != -1 && Game3.top_imgs[Game3.recent_top_index].image != el.image) {
                        if ( !Game3.top_imgs[Game3.recent_top_index].success ) {
                            Game3.Set_Opacity(Game3.top_imgs[Game3.recent_top_index].image, Game3.DEFAULT_OPACITY);
                        }
                    }
                    Game3.Set_Opacity(el.image, 1.0); // Set image opacity 100%
                    Game3.recent_top_index = el.index; // Remember index of the image currently being clicked
                }
                Game3.top_img_clicked = true; // Set flag "top_img_clicked"
            }, false)
        });

        // Add click listener to image objects in bottom row.
        this.btm_imgs.forEach(function (img) {
            img.addEventListener("click", function () {
                // Only after a top image has just been clicked, an image in bottom row can be clicked.
                if (Game3.top_img_clicked) {
                    if (Game3.Get_Opacity(img) < 1.0) {
                        Game3.Set_Opacity(img, 1.0); // Set image opacity 100%
                        if (Game3.top_imgs[Game3.recent_top_index].success) { // This top image was successfully chosen before => Ignore it.
                            Game3.Set_Opacity(img, Game3.DEFAULT_OPACITY);
                        }
                        else { // (Game3.top_imgs[Game3.recent_top_index].success) == false
                            Game3.Check_Match(img);
                        }
                    }
                    Game3.top_img_clicked = false; // Reset flag "top_img_clicked"
                }
                else { // Game3.top_img_clicked == false
                    if (Game3.recent_top_index != -1 && Game3.Get_Opacity(Game3.top_imgs[Game3.recent_top_index].image) == 1.0
                                                     && Game3.top_imgs[Game3.recent_top_index].success == false ) {
                        if (Game3.Get_Opacity(img) < 1.0) {
                            Game3.Set_Opacity(img, 1.0); // Set image opacity 100%
                        }
                        Game3.Check_Match(img, false);
                    }
                }
            }, false)
        });
    }, /* End Init */

    /*
     *
     */
    Check_Match : function (btm_img, is_top_img_clicked= true) {
        /* 
         * MATCH: If the top image and the bottom image are matched (because their file names have the same last character;
         * for example: "top-c" and "btm-c" have the same last character 'c' => MATCH), then increase the number of success attempts.
         * 
         * 5 means: 5 positions shifted to the left, because the file extension ".jpg" has 4 characters and the character left from dot (.) 
         * is the 5th position.
         */
        if ( btm_img.src.charAt(btm_img.src.length-5) == (this.top_imgs[Game3.recent_top_index].image.src).charAt(this.top_imgs[this.recent_top_index].image.src.length-5) ) {
            // Update matched top image
            this.top_imgs[this.recent_top_index].success = true; // MATCH
            this.top_imgs[this.recent_top_index].image.src = this.Get_NewImgSrc(this.top_imgs[this.recent_top_index].image);
            // Update matched bottom image
            btm_img.src = this.Get_NewImgSrc(btm_img);
            // Update counter for success attempts
            this.success_attempts += 1;
            // If finish all A,B,C,D
            if (this.success_attempts == this.DEFAULT_SUCCESS_NUMBER) {
                this.Timer_Stop();
                alert("Success.\nTime left: " + this.timer_time_left + "s.\nGoto result.php?game=" + this.success_attempts + "&timeleft=" + this.timer_time_left);
                // Goto new page
                window.location.href = "result.php?game=" + this.success_attempts + "&timeleft=" + this.timer_time_left;
                return;
            }
        }
        /*
         * DISMATCH: If the top image and the bottom image are NOT matched (because their filenames have different last character;
         * for example: "top-c" and "btm-b" have the different last characters 'c' and 'b' => NO match), then do not increase the
         * number of success attempts and set the opacity of both images to DEFAULT_OPACITY (60%) again.
         */
        else { // NO Match
            if ( !this.top_imgs[this.recent_top_index].success )
            this.Set_Opacity(this.top_imgs[this.recent_top_index].image, this.DEFAULT_OPACITY);
            if (is_top_img_clicked) {
                this.Set_Opacity(btm_img, this.DEFAULT_OPACITY);
            }
            else {
                if ( btm_img.src.indexOf('2') == -1 )
                this.Set_Opacity(btm_img, this.DEFAULT_OPACITY);
            }
        }
    }, /* End Check_Match */

    /*
     * This function change value for the [src] attribute of top/bottom image when top and bottom images match.
     */
    Get_NewImgSrc : function (img) {
        return (img.src.substr(0, img.src.length-4) // file name without extension (before dot)
              + "2" 
              + img.src.substr(-4)); // start-index (-4) is +4, counted from end to begin => file extension (after dot)
    }, /* End Change_ImgSrc */

    /*
     * This function returns the current opacity of an image. The return value is between 0 (0%) and 1.0 (100%).
     * @param:
     *      img: Image the opacity value of which is to be returned.
     * @return:
     *      Double number between 0.0 (0%) and 1.0 (100%). If the opacity is 60%, it returns 0.6 as result.
     */
    Get_Opacity : function (img) {
        return (window.getComputedStyle(img)).opacity;
    }, /* End Get_Opacity */

    /*
     * This function sets the opacity value (between 0.0 and 1.0) for an image.
     * @param:
     *      img: Image that is to be assigned an opacity.
     *      opac: Unsigned double number indicating the opacity value. For example, opac=0.6 indicates an 
     *            opacity 60% (or 0.6).
     */
    Set_Opacity : function (img, opac) {
        img.style.opacity = "" + opac;
        img.style.filter = "alpha(opacity=" + (opac * 100) + ")";
    }, /* End Set_Opacity */

    /*
     * Stop the timer
     */
    Timer_Stop : function () {
        clearInterval(this.timer_anim_id);
    },

    /*
     * Start the timer
     */
    Timer_Run : function () {
        this.timer_anim_id = setInterval(function () {
            Game3.timer_time_left = (Game3.DEFAULT_TIMER_DURATION + 1) - Math.floor((Date.now() - Game3.timer_start_millis) / 1000);
            if (Game3.timer_time_left <= 0) { // If timer finishes => 0 second left
                Game3.Timer_Stop();
                alert("Game over.");
                // Goto new page
                window.location.href = "result.php?game=" + Game3.success_attempts;
            }
            else {
                Game3.timer.innerHTML = "Timer: " + Game3.timer_time_left;
            }
        }, 1000 /* millisecond */);
    },

 };


Game3.Start();
