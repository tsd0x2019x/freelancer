/**************************\
 * PerSeq.js                |
 **************************/

// Namespace for program
var PerSeq = {
    DEFAULT_CSS_SPAN_SEQUENCE_ORIGINAL : "seq-text origin",
    DEFAULT_CSS_SPAN_SEQUENCE_FIRST_PERMUTATION : "seq-text first-permut",
    DEFAULT_CSS_TEXTAREA_SEQUENCE_ORIGINAL : ".cards-state.origin > input[type='text']",
    DEFAULT_CSS_TEXTAREA_SEQUENCE_FIRST_PERMUTATION : ".cards-state.first-permut > input[type='text']",
    DEFAULT_CSS_SOLUTION_SHOW_BUTTON : "entity-box",
    DEFAULT_CSS_SOLUTION_FRAME : "row content-frame",
    DEFAULT_PRIMES : [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97], // Set of primes

    span_origin : null, /* <span> element for sequence with origin al order */
    span_permute : null, /* <span> element for sequence with order after first change/permutation */

    sequence_origin : [1,2,3,4,5,6,7,8,9,10,11,12,13], // Array for original periodic sequence (i.e sequence with the original order), with default values,
    sequence_permute : [6,1,7,4,2,9,13,11,10,12,8,5,3], // Array for sequence after the first permutation, with default values

    cycles : [], // Set (Array) of all cycles
    cycle_cardinalities : [], // Array containing the cadinality of each cycle

    solution_show_button : null,
    solution_frame : null,
    

    /*
     * Start program
     */
    Start : function () {
        this.Load_Defaults();
        this.Initialize();
        this.Solve();
    }, /* End Start */


    /*
     * Load default values for the input text boxes.
     */
    Load_Defaults : function () {
        // First init <sequence_origin>
        document.querySelectorAll(PerSeq.DEFAULT_CSS_TEXTAREA_SEQUENCE_ORIGINAL).forEach(function (element, index) {
            element.value = (
                (PerSeq.sequence_origin[index] == 1) ? "A" :
                (PerSeq.sequence_origin[index] == 11) ? "J" : 
                (PerSeq.sequence_origin[index] == 12) ? "Q" : 
                (PerSeq.sequence_origin[index] == 13) ? "K" : 
                "" + PerSeq.sequence_origin[index]
            );
        });

        // First init <sequence_permute>
        document.querySelectorAll(PerSeq.DEFAULT_CSS_TEXTAREA_SEQUENCE_FIRST_PERMUTATION).forEach(function (element, index) {
            element.value = (
                (PerSeq.sequence_permute[index] == 1) ? "A" :
                (PerSeq.sequence_permute[index] == 11) ? "J" : 
                (PerSeq.sequence_permute[index] == 12) ? "Q" : 
                (PerSeq.sequence_permute[index] == 13) ? "K" : 
                "" + PerSeq.sequence_permute[index]
            );
        });
    },


    /*
     * Initializes the image objects in the top and bottom row. Prepare the event listeners.
     */
    Initialize : function () {
        // Get references to the <span> elements displaying the original sequence and the (first) permutation
        this.span_origin = document.getElementsByClassName(PerSeq.DEFAULT_CSS_SPAN_SEQUENCE_ORIGINAL)[0];
        this.span_permute = document.getElementsByClassName(PerSeq.DEFAULT_CSS_SPAN_SEQUENCE_FIRST_PERMUTATION)[0];

        // Get reference to the solution frame and the button for showing/hiding solution
        this.solution_frame = document.getElementsByClassName(PerSeq.DEFAULT_CSS_SOLUTION_FRAME)[0];
        this.solution_show_button = document.getElementsByClassName(PerSeq.DEFAULT_CSS_SOLUTION_SHOW_BUTTON)[0];

        // Add event listeners
        this.solution_show_button.addEventListener('click', function (evt) {
            if (this.innerHTML == "&nbsp;+&nbsp;") { // 'plus' sign
                this.innerHTML = "&nbsp;&minus;&nbsp;"
                evt.target.style.background = "#ffb3b3";
                PerSeq.solution_frame.style.visibility = "visible";
                PerSeq.solution_frame.style.display = "block"; // "inline-block";
            }
            else {
                this.innerHTML = "&nbsp;&plus;&nbsp;"
                evt.target.style.background = "rgb(187, 253, 148)";
                PerSeq.solution_frame.style.visibility = "hidden";
                PerSeq.solution_frame.style.display = "none";
            }
        });

    }, /* End Init */


    /*
     *
     */
    Solve : function () {
        // Load (new) values to to the text boxes
        this.span_origin.innerHTML = "";
        this.span_permute.innerHTML = "";
        
        document.querySelectorAll(PerSeq.DEFAULT_CSS_TEXTAREA_SEQUENCE_ORIGINAL).forEach (function(elem, idx) {
            PerSeq.span_origin.innerHTML += (idx == 0) ? ("&nbsp;" + elem.value) : (", " + elem.value);
            PerSeq.sequence_origin[idx] = (elem.value == "A") ? 1 : (elem.value == "J") ? 11 : (elem.value == "Q") ? 12 : (elem.value == "K") ? 13 : parseInt(elem.value);
        });
        
        document.querySelectorAll(PerSeq.DEFAULT_CSS_TEXTAREA_SEQUENCE_FIRST_PERMUTATION).forEach (function(elem, idx) {
            PerSeq.span_permute.innerHTML += (idx == 0) ? ("&nbsp;" + elem.value) : (", " + elem.value);
            PerSeq.sequence_permute[idx] = (elem.value == "A") ? 1 : (elem.value == "J") ? 11 : (elem.value == "Q") ? 12 : (elem.value == "K") ? 13 : parseInt(elem.value);
        });

        // Calculates cycles
        this.Calc_Cycles();

        // Update solution frame
        PerSeq.Update_Solution_Content(PerSeq.solution_frame);
    },

    /*
     * Determine and provide a cycle. The first (start) element of cycle is an element from <origin_sequence> with the index <first_index>.
     * The final result (cycle) will also be stored in the argument <cycle>.
     */
    Get_Cycle : function (cycle, origin_sequence, permute_sequence, /* Index from <origin_sequence> for the first element in cycle */ first_index = 0) {
        var index = first_index;
        if (cycle.length > 0 && cycle[0] == origin_sequence[index]) {
            return cycle.length;
        }
        cycle.push(origin_sequence[index]); // Add first element to cycle
        if (permute_sequence[index] == origin_sequence[index]) {
            return cycle.length;
        }
        index = origin_sequence.findIndex(function (element) { // Return the index of the element equals <permute_sequence[index]> in <origin_sequence>
            return (element == permute_sequence[index]);
        });
        return this.Get_Cycle(cycle, origin_sequence, permute_sequence, index);
    },


    /*
     * Check if an element <searchElement> belongs to one of the (found) cycles.
     */
    Is_In_Cycle : function (searchElement) {
        for (var index = 0; index < this.cycles.length; index++) {
            var found = this.cycles[index].find(function (elem) {
                return (elem == searchElement); // return the element in <this.cycles[index]> that is equal <searchElement>. Otherwise, return [undefined]
            });
            if (found !== undefined) {
                return true;
            }
        };
        return false;
    },


    /*
     * Calculate the Least-Common-Multiple
     */
    Least_Common_Multiple : function (theArgs) { // Array of arguments
        var biggest = 0;
        var pIndex = 0;
        var expArray = [];
        var lcm = 1; // Least-Common-Multiple

        // Determine the biggest number from <theArgs>
        theArgs.forEach(function (element) {
            if (element > biggest) { biggest = element; }
        });

        // Determine the index of the prime that is less than equals <biggest>        
        for (; pIndex < this.DEFAULT_PRIMES.length; pIndex++) {
            if (this.DEFAULT_PRIMES[pIndex] > biggest) break;
        }
        pIndex -= 1;

        // Determine greates exponent for every prime from 2..
        for (var i = 0; i <= pIndex; i++) {
            var prime = this.DEFAULT_PRIMES[i];
            var maxExp = 0;
            // Determine the max exponent
            theArgs.forEach(function (element) {
                if (element > 1) { // Consider only numbers greater than 1
                    var tmp = element;
                    var exp = 0;
                    while ((tmp % prime) == 0) { // [element] is divisible by [prime]
                        // Calculate the exponent
                        exp += 1;
                        tmp /= prime;
                    }
                    if (exp > maxExp) {
                        maxExp = exp;
                    }
                }
            });
            // Add determined max exponent to array
            expArray.push(maxExp);
        }

        // Calculate the Least-Common-Multiple
        expArray.forEach(function (exponent, index) {
            let prime = PerSeq.DEFAULT_PRIMES[index];
            if (exponent > 0) {
                lcm *= Math.pow(prime, exponent);
            }
        });

        return lcm;
    },


    /*
     * Calculate the cycles (if exist!) in the permutation and store them in <PerSeq.cycles>.
     */
    Calc_Cycles : function () {
        // Clear arrays <cycles> and <cycle_cardinalites> first
        this.cycles.length = 0;
        this.cycle_cardinalities.length = 0;

        // Try to add new cycle(s) to array
        for (var index = 0; index < this.sequence_origin.length; index++) {
            if (this.cycles.length > 0) {
                if (this.Is_In_Cycle(this.sequence_origin[index])) {
                    continue;
                }
            }
            var new_cycle = [];
            // Determine the cycle with start element as element from <PerSeq.sequence_origin> with index <index>. Store result in <cycle>.
            this.Get_Cycle(new_cycle, this.sequence_origin, this.sequence_permute, index);
            this.cycles.push(new_cycle);
            this.cycle_cardinalities.push(new_cycle.length);
        }
    },


    /*
     * Provide the next sequence with the order corresponding to the permutation (rule) given by <sequence_origin> and <sequence_permute>
     */
    Update_Order : function (/* array */ current_state) {
        let next_state = [];
        current_state.forEach((element, index) => {
            next_state.push(current_state[PerSeq.sequence_permute[index]-1]);
        });
        return next_state;
    },

    /*
     * Update the solution frame with content (maths using LaTeX)
     */
    Update_Solution_Content : function (DOMElement) {
        DOMElement.innerHTML = "The cards A, J, Q, K correspond to the numeric values 1, 11, 12, 13. That means:" 
            + "$$ A = 1,\\,J = 11,\\,Q = 12,\\,K = 13 $$"
            + "The original order and the first change can be written as the <i>permutation</i>:"
            + "$$"
            + "\\sigma := "
            + "\\begin{pmatrix}";
        this.sequence_origin.forEach(function (element, index) {
            DOMElement.innerHTML += (index == 0) ? element : (" & " + element);
        });
        DOMElement.innerHTML += "\\\\"; // new line
        this.sequence_permute.forEach(function (element, index) {
            DOMElement.innerHTML += (index == 0) ? element : (" & " + element);
        });
        DOMElement.innerHTML += "\\end{pmatrix}"
            + "$$"
            + "with the following <i>cycles</i>:"
            + "$$"
            + "\\begin{array}{l}";
        this.cycles.forEach(function (cycle, idx) { // List the cycles
            DOMElement.innerHTML += "c_{" + (idx + 1) + "} := (";
            cycle.forEach(function (element, index) { // List the element within a cycle
                DOMElement.innerHTML += (index == 0) ? element : ("\\;\\;" + element);
            });
            DOMElement.innerHTML += ") = (";
            cycle.forEach(function (element, index) { // List the element within a cycle again, with color
                DOMElement.innerHTML += (index == 0) ? ("\\color{red}{" + element + "}") : ("\\rightarrow \\color{red}{" + element + "}");
            });
            DOMElement.innerHTML += "\\rightarrow " + PerSeq.cycles[idx][0] + "\\rightarrow \\dots)";
            if (idx < (PerSeq.cycles.length - 1)) {
                DOMElement.innerHTML += " \\\\";
            }
        });
        DOMElement.innerHTML += "\\end{array}"
            + "$$"
            + "The  permutation \\( \\sigma \\) can also be represented by its cycles:"
            + "$$"
            + "\\begin{array}{rl}"
            + "\\sigma & = \\begin{pmatrix}";
        this.sequence_origin.forEach(function (element, index) {
            DOMElement.innerHTML += (index == 0) ? element : (" & " + element);
        });
        DOMElement.innerHTML += "\\\\"; // new line
        this.sequence_permute.forEach(function (element, index) {
            DOMElement.innerHTML += (index == 0) ? element : (" & " + element);
        });
        DOMElement.innerHTML += "\\end{pmatrix}\\\\"
            + "& = ";
        this.cycles.forEach(function (cycle, idx) {
            DOMElement.innerHTML += (idx == 0) ? "(" : " \\; (";
            cycle.forEach(function (element, index) { // List the element within a cycle
                DOMElement.innerHTML += (index == 0) ? element : ("\\;\\;" + element);
            });
            DOMElement.innerHTML += ")";
        });
        DOMElement.innerHTML += " \\\\"
            + "& = ";
        this.cycles.forEach(function (cycle, idx) {
            DOMElement.innerHTML += "c_" + (idx + 1) + " \\;";
        });
        DOMElement.innerHTML += "\\end{array}"
            + "$$"
            + "The <i>cycles</i> respectively have the <i>cardinalities</i> (number of elements):"
            + "$$"
            + "\\begin{array}{l}";
        this.cycles.forEach(function (cycle, idx) {
            DOMElement.innerHTML += "|c_" + (idx + 1) + "| = " + cycle.length + " \\\\";
        });
        DOMElement.innerHTML += "\\end{array}"
            + "$$"
            + "And the <i>least common multiple</i> (LCM) of those cardinalities is:"
            + "$$"
            + "LCM(";
        this.cycles.forEach(function (cycle, idx) {
            if (idx > 0) DOMElement.innerHTML += ", ";
            DOMElement.innerHTML += "|c_" + (idx + 1) + "|";
        });
        DOMElement.innerHTML += ") = ";
        this.cycles.forEach(function (cycle, idx) {
            if (idx > 0) DOMElement.innerHTML += " \\cdot";
            DOMElement.innerHTML += cycle.length;
        });
        // Calculate Least-Common-Multiple
        var lcm = this.Least_Common_Multiple(this.cycle_cardinalities);
        DOMElement.innerHTML += " = " + lcm;
        DOMElement.innerHTML += "$$"
            + "So (at least) after <b style='color:red;'>" + lcm + "</b> change(s) or feeding(s) the cards come "
            + "out in their original order.";

        /* Print the <lcm> changes from original order until the last change before the original
         * order is reached again.
         */
        DOMElement.innerHTML += "$$\\begin{array}{l}";
        DOMElement.innerHTML += "\\color{red}{0.} &";
        this.sequence_origin.forEach((element, index) => { // Original order
            let card = (
                (element == 1) ? "A" : 
                (element == 11) ? "J" :
                (element == 12) ? "Q" :
                (element == 13) ? "K" :
                element
            );
            DOMElement.innerHTML += (index == 0) ? card : (" & " + card);
        });
        DOMElement.innerHTML += "\\\\"; // newline
        let current_state = this.sequence_origin;
        for (let i = 1; i <= lcm; i++) { // Print the next order changes
            let next_state = this.Update_Order(current_state);
            DOMElement.innerHTML += "\\color{red}{" + i + "}. &";
            next_state.forEach((element, index) => {
                let card = (
                    (element == 1) ? "A" : 
                    (element == 11) ? "J" :
                    (element == 12) ? "Q" :
                    (element == 13) ? "K" :
                    element
                );
                DOMElement.innerHTML += (index == 0) ? card : " & " + card;
            });
            DOMElement.innerHTML += "\\\\";
            current_state = next_state;
        }        
        DOMElement.innerHTML += "\\end{array}$$";

        // End of proof TeX
        DOMElement.innerHTML += "\\(\\;\\;\\blacksquare \\)";

        // Causes MathJax preprocessor to reload the page
        MathJax.Hub.Typeset();
    },

 };


PerSeq.Start();
